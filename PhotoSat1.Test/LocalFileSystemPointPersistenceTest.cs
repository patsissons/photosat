﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Diagnostics.CodeAnalysis;

namespace PhotoSat1.Test
{
    [TestClass]
    public class LocalFileSystemPointPersistenceTest
    {
        [TestMethod]
        public void DefaultEmptyConstructorTest()
        {
            var persistence = new LocalFileSystemPointPersistence();

            Assert.AreEqual(Directory.GetCurrentDirectory(), persistence.JSONFile.Directory.FullName);
            Assert.AreEqual(Directory.GetCurrentDirectory(), persistence.XMLFile.Directory.FullName);

            Assert.AreEqual(LocalFileSystemPointPersistence.JSONFileName, persistence.JSONFile.Name);
            Assert.AreEqual(LocalFileSystemPointPersistence.XMLFileName, persistence.XMLFile.Name);
        }

        [TestMethod]
        public void DefaultNonEmptyConstructorTest()
        {
            var dir = new DirectoryInfo(@"C:\tmp");

            var persistence = new LocalFileSystemPointPersistence(dir.FullName);

            Assert.AreEqual(dir.FullName, persistence.JSONFile.Directory.FullName);
            Assert.AreEqual(dir.FullName, persistence.XMLFile.Directory.FullName);

            Assert.AreEqual(LocalFileSystemPointPersistence.JSONFileName, persistence.JSONFile.Name);
            Assert.AreEqual(LocalFileSystemPointPersistence.XMLFileName, persistence.XMLFile.Name);
        }

        [TestMethod]
        public void DefaultInstanceNonNullTest()
        {
            Assert.IsNotNull(LocalFileSystemPointPersistence.Default);
        }

        [TestMethod]
        public void SaveJSONAsyncTest()
        {
            LocalFileSystemPointPersistence.Default.DeleteJSONAsync().Wait();

            Assert.IsTrue(LocalFileSystemPointPersistence.Default.SaveJSONAsync(new[] { Point3D.Empty }).Result);

            ConfirmFileContent(LocalFileSystemPointPersistence.Default.JSONFile, 
@"[
  {
    ""X"": 0.0,
    ""Y"": 0.0,
    ""Z"": 0.0
  }
]");

            Assert.IsTrue(LocalFileSystemPointPersistence.Default.SaveJSONAsync(new[] { Point3D.Empty, Point3D.Empty }).Result);

            ConfirmFileContent(LocalFileSystemPointPersistence.Default.JSONFile,
@"[
  {
    ""X"": 0.0,
    ""Y"": 0.0,
    ""Z"": 0.0
  },
  {
    ""X"": 0.0,
    ""Y"": 0.0,
    ""Z"": 0.0
  }
]");
        }

        [TestMethod]
        public void SaveXMLAsyncTest()
        {
            LocalFileSystemPointPersistence.Default.DeleteXMLAsync().Wait();

            Assert.IsTrue(LocalFileSystemPointPersistence.Default.SaveXMLAsync(new[] { Point3D.Empty }).Result);

            ConfirmFileContent(LocalFileSystemPointPersistence.Default.XMLFile,
@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfPoint3D xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <Point3D>
    <X>0</X>
    <Y>0</Y>
    <Z>0</Z>
  </Point3D>
</ArrayOfPoint3D>");

            Assert.IsTrue(LocalFileSystemPointPersistence.Default.SaveXMLAsync(new[] { Point3D.Empty, Point3D.Empty }).Result);

            ConfirmFileContent(LocalFileSystemPointPersistence.Default.XMLFile,
@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfPoint3D xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <Point3D>
    <X>0</X>
    <Y>0</Y>
    <Z>0</Z>
  </Point3D>
  <Point3D>
    <X>0</X>
    <Y>0</Y>
    <Z>0</Z>
  </Point3D>
</ArrayOfPoint3D>");
        }

        [TestMethod]
        public void DeleteJSONAsyncTest()
        {
            Assert.IsTrue(LocalFileSystemPointPersistence.Default.SaveJSONAsync(new[] { Point3D.Empty }).Result);
            Assert.IsTrue(LocalFileSystemPointPersistence.Default.JSONFile.Exists);

            LocalFileSystemPointPersistence.Default.DeleteJSONAsync().Wait();
            LocalFileSystemPointPersistence.Default.JSONFile.Refresh();

            Assert.IsFalse(LocalFileSystemPointPersistence.Default.JSONFile.Exists);
        }

        [TestMethod]
        public void DeleteXMLAsyncTest()
        {
            Assert.IsTrue(LocalFileSystemPointPersistence.Default.SaveXMLAsync(new[] { Point3D.Empty }).Result);
            Assert.IsTrue(LocalFileSystemPointPersistence.Default.XMLFile.Exists);

            LocalFileSystemPointPersistence.Default.DeleteXMLAsync().Wait();
            LocalFileSystemPointPersistence.Default.XMLFile.Refresh();

            Assert.IsFalse(LocalFileSystemPointPersistence.Default.XMLFile.Exists);
        }

        [TestMethod]
        public void GetFileTest()
        {
            var file = LocalFileSystemPointPersistence.GetFile("test.file");

            Assert.AreEqual(Path.Combine(Directory.GetCurrentDirectory(), "test.file"), file.FullName);

            file = LocalFileSystemPointPersistence.GetFile("test.file", @"C:\tmp");

            Assert.AreEqual(Path.Combine(@"C:\tmp", "test.file"), file.FullName);
        }

        [TestMethod]
        public void SaveContentToFileAsyncTest()
        {
            var file = LocalFileSystemPointPersistence.GetFile("test.file");

            LocalFileSystemPointPersistence.DeleteFileAsync(file).Wait();

            Assert.IsTrue(LocalFileSystemPointPersistence.SaveContentToFileAsync(file, "testing").Result);

            ConfirmFileContent(file, "testing");

            Assert.IsTrue(LocalFileSystemPointPersistence.SaveContentToFileAsync(file, " and more testing").Result);

            ConfirmFileContent(file, " and more testing");
        }

        [TestMethod]
        public void SaveContentToFileAsyncWhenLockedTest()
        {
            var file = LocalFileSystemPointPersistence.GetFile("test.file");

            using (var fs = file.OpenRead())
            {
                // this *SHOULD NOT* throw an exception
                LocalFileSystemPointPersistence.SaveContentToFileAsync(file, "testing").Wait();
            }
        }

        [TestMethod]
        public void DeleteFileAsyncTest()
        {
            var file = LocalFileSystemPointPersistence.GetFile("test.file");

            CreateFileIfNotExists(file);

            Assert.IsTrue(file.Exists);

            LocalFileSystemPointPersistence.DeleteFileAsync(file).Wait();

            file.Refresh();

            Assert.IsFalse(file.Exists);
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        [ExcludeFromCodeCoverage]
        public void DeleteFileAsyncWhenLockedTest()
        {
            var file = LocalFileSystemPointPersistence.GetFile("test.file");

            CreateFileIfNotExists(file);

            using (var fs = file.OpenRead())
            {
                // this *SHOULD* throw an exception
                LocalFileSystemPointPersistence.DeleteFileAsync(file).GetAwaiter().GetResult();
            }
        }

        // excluded because we don't want the uncovered blocks due to the conditional branch
        [ExcludeFromCodeCoverage]
        private void CreateFileIfNotExists(FileInfo file)
        {
            if (file.Exists == false)
            {
                file.Create();
            }
        }

        private void ConfirmFileContent(FileInfo file, string content)
        {
            using (var fs = file.OpenRead())
            using (var sr = new StreamReader(fs))
            {
                Assert.AreEqual(content, sr.ReadToEnd());
            }
        }
    }
}
