﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PhotoSat1.Test
{
    [TestClass]
    public class Point3DTest
    {
        [TestMethod]
        public void DistanceToTest()
        {
            Assert.AreEqual(0, Point3D.Empty.DistanceTo(Point3D.Empty));

            Assert.AreEqual(1, Point3D.Empty.DistanceTo(new Point3D(1, 0, 0)));
            Assert.AreEqual(1, Point3D.Empty.DistanceTo(new Point3D(0, 1, 0)));
            Assert.AreEqual(1, Point3D.Empty.DistanceTo(new Point3D(0, 0, 1)));

            Assert.AreEqual(2, Point3D.Empty.DistanceTo(new Point3D(2, 0, 0)));
            Assert.AreEqual(2, Point3D.Empty.DistanceTo(new Point3D(0, 2, 0)));
            Assert.AreEqual(2, Point3D.Empty.DistanceTo(new Point3D(0, 0, 2)));

            Assert.AreEqual(26.00480, new Point3D(-7, -4, 3).DistanceTo(new Point3D(17, 6, 2.5)), 0.00001);
        }

        [TestMethod]
        public void ToStringTest()
        {
            Assert.AreEqual("(0,0,0)", Point3D.Empty.ToString());
            Assert.AreEqual("(1.23,4.56,7.89)", new Point3D(1.23, 4.56, 7.89).ToString(), "Default Format");
            Assert.AreEqual("(1.23,4.56,7.89)", new Point3D(1.23, 4.56, 7.89).ToString("G"), "G");
            Assert.AreEqual("(1.23,4.56,7.89)", new Point3D(1.23, 4.56, 7.89).ToString("F2"), "F2");
            Assert.AreEqual("(1.2,4.6,7.9)", new Point3D(1.23, 4.56, 7.89).ToString("F1"), "F1");
            Assert.AreEqual("(1,5,8)", new Point3D(1.23, 4.56, 7.89).ToString("F0"), "F0");
        }
    }
}
