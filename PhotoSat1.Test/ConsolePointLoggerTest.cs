﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PhotoSat1.Test
{
    [TestClass]
    public class ConsolePointLoggerTest
    {
        [TestMethod]
        public void LogTest()
        {
            Assert.AreEqual("(1.00,1.00,1.00) (1.73 m)", ConsolePointLogger.GetText(Point3D.Empty, new Point3D(1, 1, 1)));
        }
    }
}
