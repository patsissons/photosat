﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using PhotoSat1.Contracts;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace PhotoSat1.Test
{
    [TestClass]
    public class PointGeneratorServiceTest
    {
        [TestMethod]
        public void EmptyDefaultConstructorTest()
        {
            var svc = new PointGeneratorService();
            var po = new PrivateObject(svc);

            var waitHandle = po.GetField("WaitHandle") as ManualResetEventSlim;
            Assert.IsNotNull(waitHandle);
            Assert.IsFalse(waitHandle.IsSet);

            Assert.AreEqual(Point3D.Empty, po.GetProperty("CurrentPoint"));
            Assert.AreEqual(2d, po.GetProperty("NextPositionMaxDistance"));
            Assert.AreEqual(TimeSpan.FromMilliseconds(200), po.GetProperty("NewPositionInterval"));
            Assert.AreEqual(TimeSpan.FromMilliseconds(3000), po.GetProperty("JSONPersistInterval"));
            Assert.AreEqual(TimeSpan.FromMilliseconds(4000), po.GetProperty("XMLPersistInterval"));
            Assert.AreSame(ConsolePointLogger.Default, po.GetProperty("PointLogger"));
            Assert.AreSame(LocalFileSystemPointPersistence.Default, po.GetProperty("PointPersistence"));

            var history = po.GetProperty("History") as List<Point3D>;
            Assert.IsNotNull(history);
            Assert.AreEqual(0, history.Count);

            Assert.IsNull(po.GetProperty("GeneratorTask"));
        }

        [TestMethod]
        public void NonEmptyDefaultConstructorTest()
        {
            var svc = new PointGeneratorService(Point3D.Empty, 2, 200, 3000, 4000, ConsolePointLogger.Default, LocalFileSystemPointPersistence.Default);
            var po = new PrivateObject(svc);

            var waitHandle = po.GetField("WaitHandle") as ManualResetEventSlim;
            Assert.IsNotNull(waitHandle);
            Assert.IsFalse(waitHandle.IsSet);

            Assert.AreEqual(Point3D.Empty, po.GetProperty("CurrentPoint"));
            Assert.AreEqual(2d, po.GetProperty("NextPositionMaxDistance"));
            Assert.AreEqual(TimeSpan.FromMilliseconds(200), po.GetProperty("NewPositionInterval"));
            Assert.AreEqual(TimeSpan.FromMilliseconds(3000), po.GetProperty("JSONPersistInterval"));
            Assert.AreEqual(TimeSpan.FromMilliseconds(4000), po.GetProperty("XMLPersistInterval"));
            Assert.AreSame(ConsolePointLogger.Default, po.GetProperty("PointLogger"));
            Assert.AreSame(LocalFileSystemPointPersistence.Default, po.GetProperty("PointPersistence"));

            var history = po.GetProperty("History") as List<Point3D>;
            Assert.IsNotNull(history);
            Assert.AreEqual(0, history.Count);

            Assert.IsNull(po.GetProperty("GeneratorTask"));
        }

        [TestMethod]
        public void MainConstructorTest()
        {
            var svc = new PointGeneratorService(Point3D.Empty, 2, TimeSpan.FromMilliseconds(200), TimeSpan.FromMilliseconds(3000), TimeSpan.FromMilliseconds(4000), ConsolePointLogger.Default, LocalFileSystemPointPersistence.Default);
            var po = new PrivateObject(svc);

            var waitHandle = po.GetField("WaitHandle") as ManualResetEventSlim;
            Assert.IsNotNull(waitHandle);
            Assert.IsFalse(waitHandle.IsSet);

            Assert.AreEqual(Point3D.Empty, po.GetProperty("CurrentPoint"));
            Assert.AreEqual(2d, po.GetProperty("NextPositionMaxDistance"));
            Assert.AreEqual(TimeSpan.FromMilliseconds(200), po.GetProperty("NewPositionInterval"));
            Assert.AreEqual(TimeSpan.FromMilliseconds(3000), po.GetProperty("JSONPersistInterval"));
            Assert.AreEqual(TimeSpan.FromMilliseconds(4000), po.GetProperty("XMLPersistInterval"));
            Assert.AreSame(ConsolePointLogger.Default, po.GetProperty("PointLogger"));
            Assert.AreSame(LocalFileSystemPointPersistence.Default, po.GetProperty("PointPersistence"));

            var history = po.GetProperty("History") as List<Point3D>;
            Assert.IsNotNull(history);
            Assert.AreEqual(0, history.Count);

            Assert.IsNull(po.GetProperty("GeneratorTask"));
        }

        [TestMethod]
        public void StartAsyncTest()
        {
            var svc = new PointGeneratorService(newPositionInterval: 0, pointLogger: TestPointLogger.Default, pointPersistence: TestPointPersistence.Default);
            var po = new PrivateObject(svc);

            var task = svc.StartAsync();

            Assert.IsTrue(TestPointPersistence.Default.DeleteJSONCalled);
            Assert.IsTrue(TestPointPersistence.Default.DeleteXMLCalled);

            Assert.AreSame(task, po.GetProperty("GeneratorTask"));

            Assert.AreSame(task, svc.StartAsync());

            // NOTE: this 10ms may need to be tweeked to pass this test under slower systems
            task.Wait(10);

            Assert.IsTrue(TestPointLogger.Default.LogCounter > 0);

            svc.StopAsync().Wait();
        }

        [TestMethod]
        public void PersistenceTest()
        {
            var svc = new PointGeneratorService(Point3D.Empty, 2, 0, 0, 0, TestPointLogger.Default, TestPointPersistence.Default);

            var task = svc.StartAsync();

            task.Wait(100);

            svc.StopAsync().Wait();

            Assert.IsNotNull(TestPointPersistence.Default.SavedJSONList);
            Assert.IsTrue(TestPointPersistence.Default.SavedJSONList.Count > 0);

            Assert.IsNotNull(TestPointPersistence.Default.SavedXMLList);
            Assert.IsTrue(TestPointPersistence.Default.SavedXMLList.Count > 0);
        }

        [TestMethod]
        public void StopAsyncTest()
        {
            var svc = new PointGeneratorService(newPositionInterval: 0, pointLogger: TestPointLogger.Default, pointPersistence: TestPointPersistence.Default);
            var po = new PrivateObject(svc);

            var task = svc.StartAsync();

            Assert.AreSame(task, svc.StopAsync());
            Assert.IsNull(po.GetProperty("GeneratorTask"));

            task.Wait();

            Assert.AreNotSame(task, svc.StopAsync());

            Assert.IsTrue(svc.StopAsync().IsCompleted);
        }

        [TestMethod]
        public void GetNextPointDistanceTest()
        {
            Assert.IsTrue(2.75 > PointGeneratorService.GetNextPointDistance(2.75));

            var rng = new Random();
            Assert.IsTrue(12.75 > PointGeneratorService.GetNextPointDistance(12.75, rng));
        }

        [TestMethod]
        public void GetNextPointTest()
        {
            Assert.AreEqual(2.75, Point3D.Empty.DistanceTo(PointGeneratorService.GetNextPoint(Point3D.Empty, 2.75)), 0.005);

            var rng = new Random();

            Assert.AreEqual(12.75, Point3D.Empty.DistanceTo(PointGeneratorService.GetNextPoint(Point3D.Empty, 12.75, rng)), 0.005);
        }

        class TestPointLogger : IPointLogger
        {
            public static readonly TestPointLogger Default = new TestPointLogger();

            public int LogCounter { get; set; }

            public void Log(Point3D previous, Point3D current)
            {
                ++LogCounter;
            }
        }

        class TestPointPersistence : IPointPersistence
        {
            public static readonly TestPointPersistence Default = new TestPointPersistence();

            public List<Point3D> SavedJSONList { get; private set; }
            public List<Point3D> SavedXMLList { get; private set; }

            public bool DeleteJSONCalled { get; set; }
            public bool DeleteXMLCalled { get; set; }

            public Task<bool> SaveJSONAsync(IEnumerable<Point3D> points)
            {
                SavedJSONList = points.ToList();

                return Task.FromResult(true);
            }

            public Task<bool> SaveXMLAsync(IEnumerable<Point3D> points)
            {
                SavedXMLList = points.ToList();

                return Task.FromResult(true);
            }

            public Task DeleteJSONAsync()
            {
                DeleteJSONCalled = true;

                return Task.FromResult(true);
            }

            public Task DeleteXMLAsync()
            {
                DeleteXMLCalled = true;

                return Task.FromResult(true);
            }
        }
    }
}
