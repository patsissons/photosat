﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PhotoSat1.Test
{
    [TestClass]
    public class PointPersistenceTest
    {
        [TestMethod]
        public void GetJSONTextTest()
        {
            var points = new List<Point3D>();
            Assert.AreEqual("[]", PointPersistence.GetJSONText(points, Formatting.None));

            points.Add(Point3D.Empty);
            Assert.AreEqual("[{\"X\":0.0,\"Y\":0.0,\"Z\":0.0}]", PointPersistence.GetJSONText(points, Formatting.None));

            points.Add(new Point3D(1, 2, 3));
            Assert.AreEqual("[{\"X\":0.0,\"Y\":0.0,\"Z\":0.0},{\"X\":1.0,\"Y\":2.0,\"Z\":3.0}]", PointPersistence.GetJSONText(points, Formatting.None));
        }

        [TestMethod]
        public void GetXMLTextTest()
        {
            var points = new List<Point3D>();
            Assert.AreEqual(
@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfPoint3D xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" />"
                , PointPersistence.GetXMLText(points));

            points.Add(Point3D.Empty);
            Assert.AreEqual(
@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfPoint3D xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <Point3D>
    <X>0</X>
    <Y>0</Y>
    <Z>0</Z>
  </Point3D>
</ArrayOfPoint3D>"
                , PointPersistence.GetXMLText(points));

            points.Add(new Point3D(1, 2, 3));
            Assert.AreEqual(
@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfPoint3D xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <Point3D>
    <X>0</X>
    <Y>0</Y>
    <Z>0</Z>
  </Point3D>
  <Point3D>
    <X>1</X>
    <Y>2</Y>
    <Z>3</Z>
  </Point3D>
</ArrayOfPoint3D>"
                , PointPersistence.GetXMLText(points));
        }
    }
}
