# PhotoSat Application Exercises

## Exercise #1

The programs purpose is to create and save a list of *random* points. A point describes a 3D coordinate in meters. The program must adhere to the following specifications:

* Create a random point every `200` milliseconds
* Subsequent points must be located within a sphere with a radius of `2` meters from the previous point
* A string representation of the generated point must be emitted, along with the distance from the previous point
* The list of generated points must be saved to the hard drive every `3` seconds in JSON format
* The list of generated points must be saved to the hard drive every `4` seconds in some other format (*XML for the purposes of this implementation*)

### Evaluation Criteria

* Code Quality
* Code Efficiency
