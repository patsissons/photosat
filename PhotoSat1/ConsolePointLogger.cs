﻿using PhotoSat1.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoSat1
{
    public class ConsolePointLogger : IPointLogger
    {
        public static readonly ConsolePointLogger Default = new ConsolePointLogger();

        [ExcludeFromCodeCoverage]
        public virtual void Log(Point3D previous, Point3D current)
        {
            Console.WriteLine(GetText(previous, current));
        }

        public static string GetText(Point3D previous, Point3D current)
        {
            return string.Format("{0:F2} ({1:F2} m)", current, previous.DistanceTo(current));
        }
    }
}
