﻿using Newtonsoft.Json;
using PhotoSat1.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PhotoSat1
{
    public abstract class PointPersistence : IPointPersistence
    {
        #region Abstract Functions

        public abstract Task<bool> SaveJSONAsync(IEnumerable<Point3D> points);
        public abstract Task<bool> SaveXMLAsync(IEnumerable<Point3D> points);
        public abstract Task DeleteJSONAsync();
        public abstract Task DeleteXMLAsync();

        #endregion

        public static string GetJSONText(IEnumerable<Point3D> points, Formatting formatting = Formatting.Indented)
        {
            return JsonConvert.SerializeObject(points, formatting);
        }

        public static string GetXMLText(IEnumerable<Point3D> points)
        {
            var serializer = new XmlSerializer(typeof(List<Point3D>));

            using (var output = new StringWriter())
            {
                serializer.Serialize(output, points.ToList());

                return output.ToString();
            }
        }
    }
}
