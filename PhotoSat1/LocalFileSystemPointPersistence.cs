﻿using Newtonsoft.Json;
using PhotoSat1.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoSat1
{
    public class LocalFileSystemPointPersistence : PointPersistence
    {
        public const string JSONFileName = "points.json";
        public const string XMLFileName = "points.xml";

        public static readonly LocalFileSystemPointPersistence Default = new LocalFileSystemPointPersistence();
        
        public LocalFileSystemPointPersistence(string persistenceDirectory = null)
        {
            JSONFile = GetFile(JSONFileName, persistenceDirectory);
            XMLFile = GetFile(XMLFileName, persistenceDirectory);
        }

        public FileInfo JSONFile { get; private set; }
        public FileInfo XMLFile { get; private set; }

        public override Task<bool> SaveJSONAsync(IEnumerable<Point3D> points)
        {
            return SaveContentToFileAsync(JSONFile, GetJSONText(points));
        }

        public override Task<bool> SaveXMLAsync(IEnumerable<Point3D> points)
        {
            return SaveContentToFileAsync(XMLFile, GetXMLText(points));
        }

        public override Task DeleteJSONAsync()
        {
            return DeleteFileAsync(JSONFile);
        }

        public override Task DeleteXMLAsync()
        {
            return DeleteFileAsync(XMLFile);
        }

        public static FileInfo GetFile(string fileName, string directory = null)
        {
            return new FileInfo(Path.Combine(directory ?? Directory.GetCurrentDirectory(), fileName));
        }

        public static Task<bool> SaveContentToFileAsync(FileInfo file, string content)
        {
            return Task.Run(() =>
            {
                var written = false;

                try
                {
                    using (var fs = file.Open(FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
                    using (var sw = new StreamWriter(fs))
                    {
                        sw.Write(content);
                    }

                    written = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("WARNING: Unable to Save to File: {0}", file.FullName);
                    Console.WriteLine(e.Message);
                }

                return written;
            });
        }

        public static Task DeleteFileAsync(FileInfo file)
        {
            return Task.Run(() => file.Delete());
        }
    }
}
