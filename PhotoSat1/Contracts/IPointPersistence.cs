﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoSat1.Contracts
{
    public interface IPointPersistence
    {
        Task<bool> SaveJSONAsync(IEnumerable<Point3D> points);
        Task<bool> SaveXMLAsync(IEnumerable<Point3D> points);

        Task DeleteJSONAsync();
        Task DeleteXMLAsync();
    }
}
