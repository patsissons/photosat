﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoSat1.Contracts
{
    public interface IPointLogger
    {
        void Log(Point3D previous, Point3D current);
    }
}
