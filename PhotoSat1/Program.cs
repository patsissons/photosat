﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoSat1
{
    [ExcludeFromCodeCoverage]
    class Program
    {
        static void Main(string[] args)
        {
            var svc = new PointGeneratorService();

            svc.StartAsync();

            Console.WriteLine("Press Enter To Exit...");
            Console.WriteLine();
            Console.ReadLine();

            svc.StopAsync().Wait();
        }
    }
}
