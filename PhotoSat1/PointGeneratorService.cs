﻿using PhotoSat1.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoSat1
{
    public class PointGeneratorService
    {
        #region Fields

        private readonly ManualResetEventSlim WaitHandle = new ManualResetEventSlim(false);

        #endregion

        #region Constructors

        public PointGeneratorService(
            Point3D startingPoint = default(Point3D),
            double nextPositionMaxDistance = 2,
            int newPositionInterval = 200,
            int jsonPersistInterval = 3000,
            int xmlPersistInterval = 4000,
            IPointLogger pointLogger = null,
            IPointPersistence pointPersistence = null
        )
            : this(
                startingPoint,
                nextPositionMaxDistance,
                TimeSpan.FromMilliseconds(newPositionInterval),
                TimeSpan.FromMilliseconds(jsonPersistInterval),
                TimeSpan.FromMilliseconds(xmlPersistInterval),
                pointLogger ?? ConsolePointLogger.Default,
                pointPersistence ?? LocalFileSystemPointPersistence.Default
            )
        {
        }

        public PointGeneratorService(
            Point3D startingPoint,
            double nextPositionMaxDistance,
            TimeSpan newPositionInterval,
            TimeSpan jsonPersistInterval,
            TimeSpan xmlPersistInterval,
            IPointLogger pointLogger,
            IPointPersistence pointPersistence
        )
        {
            CurrentPoint = startingPoint;
            NextPositionMaxDistance = nextPositionMaxDistance;
            NewPositionInterval = newPositionInterval;
            JSONPersistInterval = jsonPersistInterval;
            XMLPersistInterval = xmlPersistInterval;
            PointLogger = pointLogger;
            PointPersistence = pointPersistence;

            History = new List<Point3D>();
        }

        #endregion

        #region Properties

        private Point3D CurrentPoint { get; set; }

        private double NextPositionMaxDistance { get; set; }

        private TimeSpan NewPositionInterval { get; set; }

        private TimeSpan JSONPersistInterval { get; set; }

        private TimeSpan XMLPersistInterval { get; set; }

        private IPointLogger PointLogger { get; set; }

        private IPointPersistence PointPersistence { get; set; }

        private List<Point3D> History { get; set; }

        private Task GeneratorTask { get; set; }

        #endregion

        #region Functions

        public Task StartAsync()
        {
            if (GeneratorTask == null)
            {
                var task = DeletePersistenceFilesAsync();

                GeneratorTask = task.ContinueWith(_ => Generator(), CancellationToken.None, TaskContinuationOptions.LongRunning, TaskScheduler.Default);
            }

            return GeneratorTask;
        }

        public Task StopAsync()
        {
            if (GeneratorTask != null)
            {
                WaitHandle.Set();

                var task = GeneratorTask;

                GeneratorTask = null;

                return task;
            }
            else
            {
                return Task.FromResult(false);
            }
        }

        private Task DeletePersistenceFilesAsync()
        {
            return Task.WhenAll(
                PointPersistence.DeleteJSONAsync(),
                PointPersistence.DeleteXMLAsync()
            );
        }

        private void Generator()
        {
            var jsonMarker = DateTime.Now;
            var xmlMarker = DateTime.Now;

            while (WaitHandle.IsSet == false)
            {
                Task.Delay(NewPositionInterval).Wait();

                var distance = GetNextPointDistance(NextPositionMaxDistance);

                var point = GetNextPoint(CurrentPoint, distance);

                PointLogger.Log(CurrentPoint, point);

                History.Add(point);

                CurrentPoint = point;

                if (DateTime.Now.Subtract(jsonMarker) >= JSONPersistInterval)
                {
                    PointPersistence.SaveJSONAsync(History).Wait();

                    jsonMarker = DateTime.Now;
                }

                if (DateTime.Now.Subtract(xmlMarker) >= XMLPersistInterval)
                {
                    PointPersistence.SaveXMLAsync(History).Wait();

                    xmlMarker = DateTime.Now;
                }
            }
        }

        public static double GetNextPointDistance(double maxDistance, Random rng = null)
        {
            return (rng ?? new Random()).NextDouble() * maxDistance;
        }

        // Algorithm interpreted from http://mathworld.wolfram.com/SpherePointPicking.html for random spherical coordinates on a unit sphere
        // and http://mathworld.wolfram.com/SphericalCoordinates.html for projecting spherical coordinates into cartesian coordinates
        public static Point3D GetNextPoint(Point3D start, double distance, Random rng = null)
        {
            const double PIPI = 2 * Math.PI;

            rng = rng ?? new Random();

            var u = rng.NextDouble();
            var v = rng.NextDouble();

            var theta = PIPI * u;
            var phi = Math.Acos(2 * v - 1);

            var x = start.X + (distance * Math.Sin(phi) * Math.Cos(theta));
            var y = start.Y + (distance * Math.Sin(phi) * Math.Sin(theta));
            var z = start.Z + (distance * Math.Cos(phi));

            return new Point3D(x, y, z);
        }

        #endregion
    }
}
